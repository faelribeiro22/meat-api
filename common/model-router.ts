import { Router } from './router';
import * as mongoose from 'mongoose';
import { NotFoundError } from 'restify-errors';

export abstract class ModelRouter<D extends mongoose.Document> extends Router {
    basePath: string;
    pageSize: number = 1;

    constructor(protected model: mongoose.Model<D>) {
        super();
        this.basePath = `/${model.collection.name}`;
    }

    protected prepareOne(query: mongoose.DocumentQuery<D,D>): mongoose.DocumentQuery<D,D> {
        return query;
    }

    envelope(document: any): any {
        let resource = Object.assign({_links:{}}, document.toJSON());
        resource._links.self = `${this.basePath}/${resource._id}`;
        return resource;
    }

    envelopeAll(documents: any[], options: any = {}): any {
        const resource: any = {
            _links: {
                self: `${options.url}`
            },
            items: documents,
        }
        if (options.page && options.count && options.pageSize) {
            if (options.page > 1) {
                resource._links.previous = `${this.basePath}?_page=${options.page-1}`;
            }
            const remaining = options.count - (options.page * options.pageSize);
            if (remaining > 0) {
                resource._links.next = `${this.basePath}?_page=${options.page+1}`;
            }
        } 
        return resource;
    }

    validateId = (req, res, next) => {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
            next(new NotFoundError('Document not found'));
        } else {
            next();
        }
    }
    
    findAll = async (req, res, next) => {
        try {
            let page = parseInt(req.query._page || 1);
            page = page > 0 ? page : 1;

            const skip = (page -1) * this.pageSize;
            const count = await this.model.count({}).exec();
            const users = await this.model.find().skip(skip).limit(skip);
            return this.renderAll(res, users, next, {page, count, pageSize: this.pageSize, url: req.url});
        } catch (error) {
            next(error);
        }
    }

    findById = async (req, res, next) => {
        try {
            const user = await this.prepareOne(this.model.findById(req.params.id));
            this.render(res, user, next);
        } catch (error) {
            next(error);
        }
    }

    save = async (req, res, next) => {
        try {
            let user = new this.model(req.body);
            const newUser = await user.save();
            this.render(res, newUser, next);
            
        } catch (error) {
            next(error);
        }
    }

    replace = async (req, res, next) => {
        // Isso garante que o put atualize o documento completamente
        try {
            const options = { runValidators: true, overwrite: true };
            const result = await this.model.update({_id: req.params.id}, req.body, options).exec();
            if (result.n) {
                const user = await this.model.findById(req.params.id);
                this.render(res, user, next);
            } else {
                throw new NotFoundError('Documento não encontrado.');
            }
        } catch (error) {
            next(error);
        }
    }

    update = async (req, res, next) => {
        try {
            const options = { runValidators: true, new: true };
            const user = await this.model.findByIdAndUpdate(req.params.id, req.body, options);
            return this.render(res, user, next);
        } catch (error) {
            next(error);
        }
    }

    delete = async (req, res, next) => {
        try {
            const cmdResolve: any = await this.model.remove({_id: req.params.id}).exec();
            if (cmdResolve.result.n) {
                res.send(204);
            } else {
                throw new NotFoundError('Documento não encontrado.');
            }
            return next();
        } catch (error) {
            next(error);
        }
    }
}