export const environment = {
    server: { port: process.env.SERVER_PORT || 3000 },
    security: { 
        saltRounds: process.env.SALT_ROUNDS || 10,
        apiSecret: process.env.API_SECRETS || 'meat-api-secret' 
    },
    mode: 'DEV',
    db: { url: process.env.DB_URL || 'mongodb://172.19.0.2:27017/meat-api'},
    test: {
        db: {url: process.env.DB_URL || 'mongodb://172.19.0.2:27017/meat-api-test'}
    }
}