import 'jest';
import * as request from 'supertest';
import {Server} from '../server/server';
import {environment} from '../common/environment';
import {usersRouter} from './users.router'
import {User} from './users.model';

let address: string = (<any>global).address;

test('get /users', () => {
    return request(address)
        .get('/users')
        .then(response => {
            expect(response.status).toBe(200)
            expect(response.body.items).toBeInstanceOf(Array)
        }).catch(fail)
})

test('post /users', () => {
    return request(address)
        .post('/users')
        .send({
            name: 'usuario01',
            email: 'usuario01@email.com',
            password: '123456',
            cpf: '218.079.560-24'
        })
        .then(response => {
            expect(response.status).toBe(200)
            expect(response.body._id).toBeDefined()
            expect(response.body.name).toBe('usuario01')
            expect(response.body.email).toBe('usuario01@email.com')
            expect(response.body.cpf).toBe('218.079.560-24')
            expect(response.body.password).toBeUndefined()

        }).catch(fail)
})

test('get /users/aaaa - not found', () => {
    return request(address)
    .get('/users/aaaaa')
    .then(response => {
        expect(response.status).toBe(404)
    }).catch(fail)
})

test('patch /users/:id', () => {
    return request(address)
    .post('/users')
    .send({
        name: 'usuario02',
        email: 'usuario02@email.com',
        password: '123456',
    })
    .then(response => request(address)
                      .patch(`/users/${response.body._id}`)
                      .send({
                          name: 'usuario02-patch'
                      }))
    .then(response => {
        expect(response.status).toBe(200)
        expect(response.body._id).toBeDefined()
        expect(response.body.name).toBe('usuario02-patch')
        expect(response.body.email).toBe('usuario02@email.com')
        expect(response.body.password).toBeUndefined()
    })
    .catch(fail)
})
