import * as restify from 'restify';
import { ModelRouter } from '../common/model-router'
import { NotFoundError } from 'restify-errors';
import { Review } from './reviews.model';
import * as mongoose from 'mongoose';

class ReviewsRouter extends ModelRouter<Review> {
    constructor() {
        super(Review);
    }

    envelope(document) {
        let resource = super.envelope(document);
        const restaurantId = document.restaurant._id ? document.restaurant._id : document.restaurant;
        resource._links.restaurant = `/restaurants/${restaurantId}`;
        return resource;
    } 

    // findById = async (req, res, next) => {
    //     try {
    //         const user = await this.model.findById(req.params.id)
    //             .populate('user', 'name')
    //             .populate('restaurant');
    //         this.render(res, user, next);
    //     } catch (error) {
    //         next(error);
    //     }
    // }

    protected prepareOne(query: mongoose.DocumentQuery<Review, Review>): mongoose.DocumentQuery<Review, Review> {
        return query.populate('user', 'name')
                    .populate('restaurant');
    }

    applyRoutes(application: restify.Server) {
        application.get(`${this.basePath}`, this.findAll);
        application.get(`${this.basePath}/:id`, [this.validateId, this.findById]);
        application.post(`${this.basePath}`, this.save);
    }
}

export const reviewsRouter = new ReviewsRouter();