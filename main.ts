import {Server} from './server/server';
import {usersRouter} from './users/users.router';
import {restaurantRouter} from './restaurants/restaurants.router';
import {reviewsRouter} from './reviews/reviews.router';
import {mainRouter} from './main.router';

const server = new Server();
server.bootstrap([
    usersRouter,
    restaurantRouter,
    reviewsRouter,
    mainRouter
]).then(server => {
    console.log('API as running on http://localhost:3000')
}).catch(error => {
    console.log('Server failed to start');
    console.error(error);
    process.exit(1);
});