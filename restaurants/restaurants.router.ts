import * as restify from 'restify';
import { ModelRouter } from '../common/model-router'
import { NotFoundError } from 'restify-errors';
import { Restaurant } from './restaurants.model';

class RestaurantsRouter extends ModelRouter<Restaurant> {
    constructor() {
        super(Restaurant);
    }

    envelope(document) {
        let resource = super.envelope(document);
        resource._links.menu = `${this.basePath}/${resource._id}/menu`;
        return resource;
    }

    findMenu = async (req, res, next) => {
        try {
            const restaurant = await Restaurant.findById(req.params.id, "+menu");
            if (!restaurant) {
                throw new NotFoundError('Restaurant not found');
            } else {
                res.json(restaurant.menu);
                return next();
            }
        } catch (error) {
            next(error);
        }
    }

    replaceMenu = async (req, res, next) => {
        try {
            const restaurant = await Restaurant.findById(req.params.id);
            if (!restaurant) {
                throw new NotFoundError('Restaurant not found');
            } else {
                restaurant.menu = req.body;
                const newMenuRestaurant = await restaurant.save();
                res.json(newMenuRestaurant.menu);
                return next();
            }
        } catch (error) {
            next(error);
        }
    }

    applyRoutes(application: restify.Server) {
        application.get(`${this.basePath}`, this.findAll);
        application.get(`${this.basePath}/:id`, [this.validateId, this.findById]);
        application.post(`${this.basePath}`, this.save);
        application.put(`${this.basePath}/:id`, [this.validateId, this.replace]);
        application.patch(`${this.basePath}/:id`, [this.validateId, this.update])
        application.del(`${this.basePath}/:id`, [this.validateId, this.delete]);
        application.get(`${this.basePath}/:id/menu`, [this.validateId, this.findMenu]);
        application.put(`${this.basePath}/:id/menu`, [this.validateId, this.replaceMenu])
    }
}

export const restaurantRouter = new RestaurantsRouter();