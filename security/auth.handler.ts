import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';
import {User} from '../users/users.model';
import {NotAuthorizedError} from 'restify-errors';
import {environment} from '../common/environment';

export const authenticate: restify.RequestHandler = async (req, resp, next) => {
    try {
        const { email, password } = req.body
        const user = await User.findByEmail(email, '+password')
        if (user && user.matches(password)) {
            // TODO gerar token
            const token = jwt.sign({sub: user.email, iss: 'meat-api'}, environment.security.apiSecret)
            resp.json({name: user.email, email: user.email, accessToken: token})
            return next(false)
        } else {
            return next(new NotAuthorizedError('Invalid Credentials'))
        }
    } catch (e) {
        next(e)
    }
}