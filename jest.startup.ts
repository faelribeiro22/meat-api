import * as jestCli from 'jest-cli';

import { environment } from "./common/environment";
import {Server} from './server/server';
import {usersRouter} from './users/users.router'
import {User} from './users/users.model';
import {reviewsRouter} from './reviews/reviews.router';
import {Review} from './reviews/reviews.model';

let server: Server;

const beforeAllTests = () => {
    //environment.db.url = process.env.DB_URL || 'mongodb://172.19.0.2:27017/meat-api'
    environment.server.port = process.env.SERVER_PORT || 3001
    environment.mode = 'TEST';
    environment.db.url = environment.test.db.url;
    server = new Server();
    return server.bootstrap([usersRouter, reviewsRouter])
                 .then(() => Review.remove({}).exec())
                 .then(() => User.remove({}).exec())
}

const afterAllTests = () => {
    return server.shutDown();
}

beforeAllTests()
.then(() => jestCli.run())
.then(() => afterAllTests())
.catch(console.error)